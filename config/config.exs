# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ci_board,
  ecto_repos: [CiBoard.Repo],
  generators: [binary_id: true]

# Configures the endpoint
config :ci_board, CiBoardWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "USw/yWMpYOFTKhu8ymIfcH7vZRw2s5TkJTfCHTIcvUTQ/sf9FSVjKVrih4IH2Vv9",
  render_errors: [view: CiBoardWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CiBoard.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
