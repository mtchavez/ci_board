FROM elixir:1.8.0

RUN set -xe \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    build-essential \
    curl \
    inotify-tools \
    postgresql-client

RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs --no-install-recommends

ENV APP_DIR /app
RUN mkdir -p $APP_DIR

COPY . $APP_DIR

WORKDIR $APP_DIR

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix compile

ENTRYPOINT ["mix"]
