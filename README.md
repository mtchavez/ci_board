# CiBoard

[![build status](https://gitlab.com/mtchavez/ci_board/badges/master/build.svg)](https://gitlab.com/mtchavez/ci_board/commits/master)
[![coverage report](https://gitlab.com/mtchavez/ci_board/badges/master/coverage.svg)](https://gitlab.com/mtchavez/ci_board/commits/master)

A basic CI board to visualize CI status for all your projects

## Setup

- Build app image `docker-compose build`
- Create/migrate DB with `docker-compose run --rm -it mix ecto.create`
- Install Node.js dependencies with `cd assets && npm install`
- Bring up app with dependencies `docker-compose up -d app`
- Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


## Testing

- Create/migrate DB with `docker-compose run --rm -it -e MIX_ENV=test mix ecto.create`
- Run tests `docker-compose run --rm -it mix test`
