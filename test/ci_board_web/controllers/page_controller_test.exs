defmodule CiBoardWeb.PageControllerTest do
  use CiBoardWeb.ConnCase
  import CiBoard.Factory

  describe "#index with projects" do
    setup [:create_projects]

    test "displays all projects", %{conn: conn, projects: projects} do
      conn = get(conn, "/")
      Enum.each(projects, fn project ->
        assert html_response(conn, 200) =~ project.name
      end)
    end
  end

  describe "#index without projects" do
    test "displays home link" do
      conn = get(conn, "/")
      html = html_response(conn, 200)
      home_link_text = Floki.find(html, "a[href=\"#{Routes.page_path(conn, :index)}\"")
        |> Floki.text
      assert home_link_text == "home"
    end

    test "displays new project link" do
      conn = get(conn, "/")
      html = html_response(conn, 200)
      home_link_text = Floki.find(html, "a[href=\"#{Routes.project_path(conn, :new)}\"")
        |> Floki.text
      assert home_link_text == "New Project"
    end
  end

  defp create_projects(_) do
    {:ok, projects: insert_list(5, :project)}
  end
end
