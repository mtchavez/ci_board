defmodule CiBoard.BoardsTest do
  use CiBoard.DataCase

  alias CiBoard.Boards

  describe "projects" do
    alias CiBoard.Boards.Project

    @valid_attrs %{description: "some description", name: "some name", short_code: "some short_code"}
    @update_attrs %{description: "some updated description", name: "some updated name", short_code: "some updated short_code"}
    @invalid_attrs %{description: nil, name: nil, short_code: nil}

    def project_fixture(attrs \\ %{}) do
      {:ok, project} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Boards.create_project()

      project
    end

    test "list_projects/0 returns all projects" do
      project = project_fixture()
      assert Boards.list_projects() == [project]
    end

    test "get_project!/1 returns the project with given id" do
      project = project_fixture()
      assert Boards.get_project!(project.id) == project
    end

    test "create_project/1 with valid data creates a project" do
      assert {:ok, %Project{} = project} = Boards.create_project(@valid_attrs)
      assert project.description == "some description"
      assert project.name == "some name"
      assert project.short_code == "some short_code"
    end

    test "create_project/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Boards.create_project(@invalid_attrs)
    end

    test "update_project/2 with valid data updates the project" do
      project = project_fixture()
      assert {:ok, %Project{} = project} = Boards.update_project(project, @update_attrs)
      assert project.description == "some updated description"
      assert project.name == "some updated name"
      assert project.short_code == "some updated short_code"
    end

    test "update_project/2 with invalid data returns error changeset" do
      project = project_fixture()
      assert {:error, %Ecto.Changeset{}} = Boards.update_project(project, @invalid_attrs)
      assert project == Boards.get_project!(project.id)
    end

    test "delete_project/1 deletes the project" do
      project = project_fixture()
      assert {:ok, %Project{}} = Boards.delete_project(project)
      assert_raise Ecto.NoResultsError, fn -> Boards.get_project!(project.id) end
    end

    test "change_project/1 returns a project changeset" do
      project = project_fixture()
      assert %Ecto.Changeset{} = Boards.change_project(project)
    end
  end
end
