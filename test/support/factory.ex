defmodule CiBoard.Factory do
  # with Ecto
  use ExMachina.Ecto, repo: CiBoard.Repo

  def project_factory do
    %CiBoard.Boards.Project{
      name: sequence(:name, &"Project #{&1}"),
      description: "Project description",
      name: sequence(:short_code, &"proj#{&1}")
    }
  end
end
