defmodule CiBoardWeb.PageController do
  use CiBoardWeb, :controller

  alias CiBoard.Boards
  alias CiBoard.Boards.Project

  def index(conn, _params) do
    projects = Boards.list_projects()
    render(conn, "index.html", projects: projects)
  end
end
