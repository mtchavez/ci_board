defmodule CiBoard.Repo do
  use Ecto.Repo,
    otp_app: :ci_board,
    adapter: Ecto.Adapters.Postgres
end
